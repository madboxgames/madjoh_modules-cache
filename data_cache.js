define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'settings/game/game'
],
function(require, CustomEvents, GameSettings){
	var DataCache = {
		init : function(){
			if(!document.cache) 	document.cache = {};
			if(!document.cacheKeys){
				var cacheKeys = localStorage.getItem(GameSettings.gameBundle + '.cache');
				document.cacheKeys = cacheKeys ? JSON.parse(cacheKeys) : [];
			}
		},
		set : function(key, data, noStorage){
			var now = new Date().getTime();
			var cachedData = {
				date : now,
				data : data
			};
			document.cache[key] = cachedData;
			if(!noStorage) localStorage.setItem(GameSettings.gameBundle + '.cache.' + key, JSON.stringify(cachedData));
			if(document.cacheKeys.indexOf(key) === -1){
				document.cacheKeys.push(key);
				if(!noStorage) localStorage.setItem(GameSettings.gameBundle + '.cache', JSON.stringify(document.cacheKeys));
			}
		},
		get : function(key, delay){
			if(!delay) delay = 1000 * 3600 * 24; // 1 Day
			var now = new Date().getTime();
			var cachedData = document.cache[key];
			if(!cachedData){
				cachedData = localStorage.getItem(GameSettings.gameBundle + '.cache.' + key);
				if(!cachedData) return null;
				cachedData = JSON.parse(cachedData);
			}
			
			var date = cachedData.date;
			if(delay === -1 || now - date < delay) return cachedData.data;
			else return null;
		},
		clear : function(key){
			if(typeof key === 'object'){
				for(var i = 0; i < key.length; i++) DataCache.clear(key[i]);
				return;
			}
			delete document.cache[key];
			localStorage.removeItem(GameSettings.gameBundle + '.cache.' + key);
		},
		clearAll : function(){
			for(var i = 0; i < document.cacheKeys.length; i++) DataCache.clear(document.cacheKeys[i]);
			document.cache = {};
		},
		clearOnPageLeave : function(page, key){
			var eventId = CustomEvents.addCustomEventListener(page, 'HIDE', function(){
				CustomEvents.removeCustomEventListener(page, 'HIDE', eventId);
				DataCache.clear(key);
			});
		}
	};

	DataCache.init();

	return DataCache;
});